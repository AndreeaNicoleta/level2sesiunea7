/********************************************************************************
** Form generated from reading UI file 'dspmainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DSPMAINWINDOW_H
#define UI_DSPMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_DSPMainWindow
{
public:
    QWidget *centralWidget;
    QCustomPlot *plot;
    QLabel *label;
    QSpinBox *spinBox;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QSpinBox *spinBox_2;
    QSpinBox *spinBox_3;
    QSpinBox *spinBox_4;
    QSpinBox *spinBox_5;
    QPushButton *pushButton;
    QCustomPlot *plot_2;
    QLabel *label_6;
    QLabel *label_7;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *DSPMainWindow)
    {
        if (DSPMainWindow->objectName().isEmpty())
            DSPMainWindow->setObjectName(QStringLiteral("DSPMainWindow"));
        DSPMainWindow->resize(1044, 700);
        centralWidget = new QWidget(DSPMainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        plot = new QCustomPlot(centralWidget);
        plot->setObjectName(QStringLiteral("plot"));
        plot->setGeometry(QRect(9, 29, 771, 291));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(830, 30, 141, 20));
        spinBox = new QSpinBox(centralWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setGeometry(QRect(830, 60, 42, 22));
        spinBox->setMaximum(2000);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(830, 90, 141, 20));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(830, 150, 141, 20));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(830, 210, 141, 20));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(830, 260, 141, 20));
        spinBox_2 = new QSpinBox(centralWidget);
        spinBox_2->setObjectName(QStringLiteral("spinBox_2"));
        spinBox_2->setGeometry(QRect(830, 120, 42, 22));
        spinBox_2->setMaximum(2000);
        spinBox_3 = new QSpinBox(centralWidget);
        spinBox_3->setObjectName(QStringLiteral("spinBox_3"));
        spinBox_3->setGeometry(QRect(830, 180, 42, 22));
        spinBox_3->setMaximum(2000);
        spinBox_4 = new QSpinBox(centralWidget);
        spinBox_4->setObjectName(QStringLiteral("spinBox_4"));
        spinBox_4->setGeometry(QRect(830, 230, 42, 22));
        spinBox_4->setMaximum(2000);
        spinBox_5 = new QSpinBox(centralWidget);
        spinBox_5->setObjectName(QStringLiteral("spinBox_5"));
        spinBox_5->setGeometry(QRect(830, 290, 42, 22));
        spinBox_5->setMaximum(2000);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(890, 350, 75, 23));
        plot_2 = new QCustomPlot(centralWidget);
        plot_2->setObjectName(QStringLiteral("plot_2"));
        plot_2->setGeometry(QRect(10, 360, 771, 311));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(20, 340, 101, 16));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(20, 10, 81, 16));
        DSPMainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(DSPMainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1044, 21));
        DSPMainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(DSPMainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        DSPMainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(DSPMainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        DSPMainWindow->setStatusBar(statusBar);

        retranslateUi(DSPMainWindow);

        QMetaObject::connectSlotsByName(DSPMainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *DSPMainWindow)
    {
        DSPMainWindow->setWindowTitle(QApplication::translate("DSPMainWindow", "DSPMainWindow", Q_NULLPTR));
        label->setText(QApplication::translate("DSPMainWindow", "Frequency for sine wave 1:", Q_NULLPTR));
        label_2->setText(QApplication::translate("DSPMainWindow", "Frequency for sine wave 2:", Q_NULLPTR));
        label_3->setText(QApplication::translate("DSPMainWindow", "Frequency for sine wave 3:", Q_NULLPTR));
        label_4->setText(QApplication::translate("DSPMainWindow", "Frequency for sine wave 4:", Q_NULLPTR));
        label_5->setText(QApplication::translate("DSPMainWindow", "Frequency for sine wave 5:", Q_NULLPTR));
        pushButton->setText(QApplication::translate("DSPMainWindow", "Plot", Q_NULLPTR));
        label_6->setText(QApplication::translate("DSPMainWindow", "Frequency domain ", Q_NULLPTR));
        label_7->setText(QApplication::translate("DSPMainWindow", "Sine wave", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DSPMainWindow: public Ui_DSPMainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DSPMAINWINDOW_H
