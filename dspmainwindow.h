#ifndef DSPMAINWINDOW_H
#define DSPMAINWINDOW_H

#include <QMainWindow>
#include <qvector.h>

namespace Ui {
	class DSPMainWindow;
}

class DSPMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit DSPMainWindow(QWidget *parent = 0);
	~DSPMainWindow();
	QVector<double> sineWave(double valFrequency, QVector<double> xAxis);
	private slots:
	void mf_OkButton();

private:
	Ui::DSPMainWindow *ui;
};

#endif // DSPMAINWINDOW_H
