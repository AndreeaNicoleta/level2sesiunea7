#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::DSPMainWindow)
{
	ui->setupUi(this);
	QObject::connect(ui->pushButton, SIGNAL(released()), this, SLOT(mf_OkButton()));

}

DSPMainWindow::~DSPMainWindow()
{
	delete ui;
}
QVector<double> DSPMainWindow::sineWave(double valFrequency, QVector<double> xAxis)
{
	QVector<double>yAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		yAxis[i] = 1 * sin(2 * M_PI * valFrequency * xAxis[i]);
	}
	return yAxis;
}

void DSPMainWindow::mf_OkButton()
{
	QVector<double> xAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}
	QVector<double>yAxisAdder(44100);
	QVector<double>yAxis1(44100);
	QVector<double>yAxis2(44100);
	QVector<double>yAxis3(44100);
	QVector<double>yAxis4(44100);
	QVector<double>yAxis5(44100);
	
	yAxis1 = sineWave(ui->spinBox->value(), xAxis);
	yAxis2 = sineWave(ui->spinBox_2->value(), xAxis);
	yAxis3 = sineWave(ui->spinBox_3->value(), xAxis);
	yAxis4 = sineWave(ui->spinBox_4->value(), xAxis);
	yAxis5 = sineWave(ui->spinBox_5->value(), xAxis);
	for (int i = 0; i < 44100; i++)
	{
		yAxisAdder[i] = yAxis1[i] + yAxis2[i] + yAxis3[i] + yAxis4[i] + yAxis5[i];
	}

	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(xAxis, yAxisAdder);
	ui->plot->xAxis->setRange(0, 0.1);
	ui->plot->yAxis->setRange(-10, 10);
	ui->plot->replot();

	//For second plot
	int FFTSize = 128;
	double frequencyRes = 44100 / FFTSize;
	auto fft = Aquila::FftFactory::getFft(FFTSize);
	auto spectrum = fft->fft(yAxisAdder.toStdVector().data());

	QVector<double> amplitudine, frequency;

	int index = 0;
	for each(auto item in spectrum)
	{
		amplitudine.push_back(pow(item.real(), 2) * pow(item.imag(), 2));
		frequency.push_back((index++)*frequencyRes);
	}
	ui->plot_2->addGraph();
	ui->plot_2->graph()->setLineStyle(QCPGraph::LineStyle::lsImpulse);
	ui->plot_2->graph(0)->setData(frequency, amplitudine);
	ui->plot_2->xAxis->setRange(20, 20000);
	ui->plot_2->yAxis->setRange(0, *std::max_element(amplitudine.begin(), amplitudine.end()));
	ui->plot_2->replot();
}
